import cache.CustomCache;
import cache.enums.Strategies;

public class Main {

    public static void main(String[] args) {
        CustomCache customCache = new CustomCache(3, Strategies.LRU);

        customCache.put(1,1);
        customCache.put(2,2);
        customCache.put(3,3);
        System.out.println(customCache.get(1));
        customCache.put(4,4);
        System.out.println(customCache.get(2));
        System.out.println(customCache.get(1));

       // 1. null check
        /**
         * 1 addNode(key value)
         * a. null check
         * 2 remove(key)
         * a. null check
         *
         * 3. remove()
         * dont remove node if no node present
         *
         * 1. put(k,v)
         * a. inserting new key new value
         * b. inserting same key, same value
         * c. inserting same key, diff value
         * d. inserting null key
         * e. inserting null key - it should'nt be allowed
         * f. p 1, p2 g1 p3 - g2 - null or g1 == 1
         * g. updatee / insert map but not in list
         *
         * 2. get(key)
         * a. fetching new key - null
         * b. fetching existing key - null / result based on access order eviction policy
         * c. updatee / insert map but not in list
         *
         *
         */
    }

    /**
     *
     *
     * put(key, value) O(1)
     * get(key)
     * capcacity
     * remove()
     *
     * Map<K,V>
     *     DoublyLinkedList
     */
}
