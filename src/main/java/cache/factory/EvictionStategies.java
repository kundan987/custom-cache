package cache.factory;

public interface EvictionStategies<Key, Value> {

    public void put(Key key, Value value);
    public Value get(Key key);
}
