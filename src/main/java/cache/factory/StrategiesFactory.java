package cache.factory;

import cache.enums.Strategies;

public class StrategiesFactory {


    public EvictionStategies getEvictionStrategies(Strategies strategies, int capacity){

        if(Strategies.LRU.equals(strategies))
            return new LRUEvictionStrategies(capacity);
        else if (Strategies.LFU.equals(strategies))
            return new LFUEvictionStrategies();

        return new LRUEvictionStrategies(capacity);
    }
}
