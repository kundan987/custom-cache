package cache.factory;

import algorithm.DoublyLinkedList;
import algorithm.Node;

import java.util.HashMap;
import java.util.Map;

public class LRUEvictionStrategies<Key, Value> implements EvictionStategies<Key, Value> {

    private final Map<Key, Node> map;

    private final DoublyLinkedList list;

    private final int capacity;

    public LRUEvictionStrategies(int capacity) {
        map = new HashMap<Key, Node>(capacity);
        list = new DoublyLinkedList();
        this.capacity = capacity;
    }

    public void put(Key key, Value value) {
        Node node = null;
        if(map.containsKey(key)){
            node = map.get(key);
            list.remove(node);
            list.addNode(node);
        } else {
            node = new Node(key, value);
            if(map.size()>= capacity){
                Key k = (Key)list.remove();
                map.remove(k);
            }
            list.addNode(node);
        }
        map.put(key, node);
    }

    public Value get(Key key) {
        if(map.containsKey(key)){
            Node node = map.get(key);
            list.remove(node);
            list.addNode(node);
            return (Value) node.getValue();
        } else {
            return null;
        }
    }
}
