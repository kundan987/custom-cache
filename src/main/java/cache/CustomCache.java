package cache;

import cache.enums.Strategies;
import cache.factory.EvictionStategies;
import cache.factory.StrategiesFactory;


public class CustomCache<Key, Value> {

    private final EvictionStategies evictionStategies;


    public CustomCache(int capacity, Strategies strategies) {
        StrategiesFactory factory = new StrategiesFactory();
        evictionStategies = factory.getEvictionStrategies(strategies, capacity);
    }

    public void put(Key key, Value value){
        evictionStategies.put(key, value);
    }

    public Value get(Key key){
        return (Value) evictionStategies.get(key);
    }
}
