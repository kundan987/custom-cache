package algorithm;

public class Node<Key, Value> {

    Key key;
    Value value;
    Node next;
    Node prev;

    public Value getValue() {
        return value;
    }

    public Node(){

    }

    public Node(Key key, Value value) {
        this.key = key;
        this.value = value;
    }


}
