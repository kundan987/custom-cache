package algorithm;

public class DoublyLinkedList<Key, Value> {

    Node head;
    Node tail;

    public DoublyLinkedList(){
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.prev = head;

        //h<->1<->2<->t
    }
    /**
     *  head-b-tail
     *  head<->a<->b-tail
     *  head.next.prev = a;
     *  a.next = head.next;
     *  head.next = a;
     *  a.prev = head
     *
     *
     */
    public void addNode(Node node){
        head.next.prev = node;
        node.next = head.next;
        head.next = node;
        node.prev = head;


    }
    /**
     *  a-b-c O(1)
     *  a.next = c
     *  b.prev.next = b.next
     *  c.prev = a
     *  b.next.prev = b.prev
     *
     */
    public void remove(Node node){
        node.next.prev = node.prev;
        node.prev.next = node.next;
    }

    public Key remove(){
        Node node = tail.prev;
        if(node == head)
            return null;
        Key key = (Key) node.key;
        remove(node);
        return key;
    }


}
